module.exports = {
    user: {
        roles: {
            ADMIN: "ADMIN",
            USER: "USER",
            GM: "GM"
        },
        Departments: {
            HR: 'HR',
            OPERATIONS: 'OPERATIONS',
            RECRUITMENT: 'RECRUITMENT'
        },
        accessTypes: ['READ', 'READ_WRITE'],
        defaultPassword: "Word2Pass"
    },
    sop: {
        steps: {
            WORK_INSTRUCTION: 'WORK INSTRUCTION',
            GUIDELINE: 'GUIDELINE',
            CHECKLIST: 'CHECKLIST'
        }
    },
    parentType: {
        PROCESS: 'Process',
        SOP: 'SOP',
        STEP: 'Step',
        NOTE: 'note',
        DEPARTMENT: 'department',
        USER: 'user',
        ATTACHMENT: 'attachment',
        ISSUE: 'issue',
        TEMPLATE: 'template',
        NOT_DEFINED: 'not defined'
    },
    environments: {
        DEVELOPEMENT: 'development', //on local system
        TESTING: 'testing'
    },
    type: {
        NOTE: 'NOTE',
        ISSUE: 'ISSUE'
    },
    issue: {
        priority: {
            LOW: 'LOW',
            NORMAL: 'NORMAL',
            HIGH: 'HIGH'
        },
        status: {
            OPEN: 'OPEN',
            IN_PROGRESS: 'IN PROGRESS',
            REJECTED: 'REJECTED',
            READY_FOR_TEST: 'READY FOR TEST',
            CLOSED: 'CLOSED',
            POSTPONED: 'POSTPONED'
        }
    },
    APPLICATIONS:{
        DISPATCH:'DISPATCH'
    }

};