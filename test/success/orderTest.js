/* global it */
/* global describe */
var root = require('./root');
var request = require('supertest');
var randomString = require('random-string');
var casual = require('casual');

var random = randomString({
    length: 5,
    letters: true
});


describe('Order', function () {

    it("Create Order", function (done) {
        this.timeout(root.t);
        request(root.url)
            .post('orders')
            .set('Authorization', 'Bearer ' + root.adminUser.accessToken)
            .send({
                name:casual.title,
                account_id: casual.integer(from = 0, to = 1000),
                content: [{
                    space_id: root.space._id,
                    duration: casual.title,
                    price: casual.integer(from = -1000, to = 1000)
                }],
                total_price: casual.integer(from = -1000, to = 1000),
                isPaid: casual.coin_flip,
                customer_note: casual.title,
                staff_note: casual.title
            })

            .expect(200)
            .end(done);
    });

    it('Get Orders', function (done) {
        this.timeout(root.t);
        request(root.url)
            .get('orders')
            .set('Authorization', 'Bearer ' + root.adminUser.accessToken)
            .expect(200)
            .end(done);
    });

    it("Update Order", function (done) {
        this.timeout(root.t);

        request(root.url)
            .put(('orders/' + root.order._id))
            .set('Authorization', 'Bearer ' + root.adminUser.accessToken)
            .send({
                name:casual.title,
                account_id: casual.integer(from = -1000, to = 1000),
                content: [{
                    space_id: root.space._id,
                    duration: casual.title,
                    price: casual.integer(from = -1000, to = 1000)
                }],
                total_price: casual.integer(from = -1000, to = 1000),
                isPaid: casual.coin_flip,
                customer_note: casual.title,
                staff_note: casual.title
            })
            .expect(200)
            .end(done);

    });

    it("Get Order", function (done) {
        this.timeout(root.t);

        request(root.url)
            .get('orders/' + root.order._id)
            .set('Authorization', 'Bearer ' + root.adminUser.accessToken)
            .expect(200)
            .end(done);

    });


});