/* global it */
/* global describe */
var root = require('./root');
var request = require('supertest');
var randomString = require('random-string');
var casual = require('casual');

var random = randomString({
    length: 5,
    letters: true
});


describe('Space', function () {

    it("Create Space", function (done) {
        this.timeout(root.t);
        request(root.url)
            .post('spaces')
            .set('Authorization', 'Bearer ' + root.adminUser.accessToken)
            .send({
                height: casual.double(from = -1000, to = 1000),
                width: casual.double(from = -1000, to = 1000),
                min_height: casual.double(from = -1000, to = 1000),
                max_height: casual.double(from = -1000, to = 1000),
                max_width: casual.double(from = -1000, to = 1000),
                min_width: casual.double(from = -1000, to = 1000),
                mediaType: casual.title,
                advertType: casual.description,
                aspectRatio: casual.title,
                weatherMood: casual.title
            })

            .expect(200)
            .end(done);
    });

    it('Get Spaces', function (done) {
        this.timeout(root.t);
        request(root.url)
            .get('spaces')
            .set('Authorization', 'Bearer ' + root.adminUser.accessToken)
            .expect(200)
            .end(done);
    });

    it("Update Space", function (done) {
        this.timeout(root.t);

         request(root.url)
            .put(('spaces/' + root.space._id))
            .set('Authorization', 'Bearer ' + root.adminUser.accessToken)
            .send({
                height: casual.double(from = -1000, to = 1000),
                width: casual.double(from = -1000, to = 1000),
                min_height: casual.double(from = -1000, to = 1000),
                max_height: casual.double(from = -1000, to = 1000),
                max_width: casual.double(from = -1000, to = 1000),
                min_width: casual.double(from = -1000, to = 1000),
                mediaType: casual.title,
                advertType: casual.description,
                aspectRatio: casual.title,
                weatherMood: casual.title
            })
            .expect(200)
            .end(done);

    });

    it("Get Space", function (done) {
        this.timeout(root.t);

         request(root.url)
            .get('spaces/' + root.space._id)
            .set('Authorization', 'Bearer ' + root.adminUser.accessToken)
            .expect(200)
            .end(done);

    });


});