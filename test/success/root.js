/* global beforeEach */

var request = require('supertest');
var config = require('../../config');
var _ = require('lodash');
var randomString = require('random-string');
var casual = require('casual');
var random = casual.integer(4, 100000000);
var constants = require('../../constants/constants');

exports.url = config.baseURL + "api/v1/";
exports.t = 30000;

exports.adminUser = {
    userName: 'adminUser',
    firstName: 'admin',
    LastName: 'User',
    email: 'admin@app-sales.com',
    password: 'Abcd1234'
};


beforeEach(function (done) { //signin

    this.timeout(exports.t);

    if (exports.adminUser.accessToken) {
        return done();
    }

    request(exports.url)

        .post('account/login')
        .send({
            loginType: exports.adminUser.userName,
            password: exports.adminUser.password
        })
        .expect(200)
        .end(function (err, res) {
            if (err)
                done(err);
            else {
                exports.adminUser = _.extend(exports.adminUser, res.body);

                if (exports.space) {
                    return done();
                }
                request(exports.url)
                    .post('spaces')
                    .set('Authorization', 'Bearer ' + exports.adminUser.accessToken)
                    .send({
                        height: casual.double(from = -1000, to = 1000),
                        width: casual.double(from = -1000, to = 1000),
                        min_height: casual.double(from = -1000, to = 1000),
                        max_height: casual.double(from = -1000, to = 1000),
                        max_width: casual.double(from = -1000, to = 1000),
                        min_width: casual.double(from = -1000, to = 1000),
                        mediaType: casual.title,
                        advertType: casual.description,
                        aspectRatio: casual.title,
                        weatherMood: casual.title
                    })

                    .expect(200)
                    .end(function (err, result) {
                        if (err)
                            done(err);
                        else {
                            exports.space = result.body;

                            if (exports.order) {
                                return done();
                            }
                            request(exports.url)
                                .post('orders')
                                .set('Authorization', 'Bearer ' + exports.adminUser.accessToken)
                                .send({
                                    name: casual.title,
                                    account_id: casual.integer(from = 0, to = 1000),
                                    content: [{
                                        space_id: exports.space._id,
                                        duration: casual.title,
                                        price: casual.integer(from = -1000, to = 1000)
                                    }],
                                    total_price: casual.integer(from = -1000, to = 1000),
                                    isPaid: casual.coin_flip,
                                    customer_note: casual.title,
                                    staff_note: casual.title
                                })

                                .expect(200)
                                .end(function (err, result) {

                                    if (err)
                                        done(err);
                                    else {
                                        exports.order = result.body;
                                        done();
                                    }
                                });
                        }
                    });
            }

        });
});