var request = require('supertest');
var config = require('../config');
var User = require('../models/user');
var Role = require('../models/role');
var mongoose = require('mongoose');

var rID = null;
var constants = require('../constants/constants');

exports.url = config.baseURL + "/api/v1/";
exports.t = 3000;

var adminUser = {
    userName: 'adminUser',
    firstName: 'Tonny',
    lastName: 'Smith',
    email: 'admin@app-sales.com',
    password: 'Abcd1234',
    active: true,
    primaryContact: '032231',
    skype: 'TonnySmith.ts',
    role: ""
};
var adminRole = {
    name: constants.user.roles.ADMIN
};

var workerUser = {
    userName: 'John',
    firstName: 'Doe',
    lastName: 'Clark',
    email: 'worker@app-sales.com',
    password: 'Abcd1234',
    active: true,
    primaryContact: '032231',
    skype: 'TonnySmith.ts',
    role: ""
};
var workerRole = {
    name: constants.user.roles.USER
};

if (process.env.ENV === 'development') {
    console.log('dev');
    mongoose.connect('mongodb://localhost:27017/sales');
} else {
    console.log('local');
   
    mongoose.connect('mongodb://localhost:27017/sales');
}

var db = mongoose.connection;

//open mongoose connection and reset user collection 


if (config.ENV == 'development') {
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function (err) {
        if (err) throw err;
        else {
            mongoose.connection.db.dropDatabase(function (err) {

                if (err) throw err;
                else {
                    console.log('Items gone');

                }
            });

        }
    });


    describe('Reset', function () {
        this.timeout(900000);


        it('Create SuperAdmin', function (done) {
            this.timeout(900000);
            var role = new Role(adminRole);
            role.save(role, function (err, res) {
                if (err) console.error(err);
                adminUser.role = res._id;
                var user = new User(adminUser);
                return user.save(user, function (err, res) {
                    if (err) console.error(err);
                    done(err);
                });
            });
        });

        it('Create Worker', function (done) {
            this.timeout(900000);
            var role = new Role(workerRole);
            role.save(role, function (err, res) {
                if (err) console.error(err);
                workerUser.role = res._id;
                var user = new User(workerUser);
                return user.save(user, function (err, res) {
                    if (err) console.error(err);
                    done(err);
                });
            });
        });

    });
}