var express = require('express');
var app = express();
var cookieParser = require('cookie-parser');
var path = require('path');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var config = require('./config.js');
var constants = require('./constants/constants');
var UserController = require('./controllers/userController');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
var authUtil = require('./utils/auth');
var fs = require('fs');
var router = express.Router();
var Error = require('./utils/error.messages');
passport.use(new BearerStrategy(UserController.findByToken));
var tokenAuth = passport.authenticate('bearer', {
    session: false
});

var cluster = require('cluster');
var numCPUs = require('os').cpus().length;
var morgan = require('morgan');
var passport = require('passport');

// Routes
var users = require('./routes/userApi');
var spaces = require('./routes/spaceApi');
var roles = require('./routes/roleApi');
var orders = require('./routes/orderApi');
var compression = require('compression');

app.use(compression());

if (cluster.isMaster && config.ENV === constants.environments.TESTING) {
    // Fork workers.
    for (var i = 0; i < numCPUs; i++) {
        console.info('cluster ' + i + ' is online');
        cluster.fork();
    }

    cluster.on('exit', function (worker, code, signal) {
        console.log("worker " + worker.process.pid + " died");
    });
} else {

    if (config.ENV === constants.environments.TESTING) {
        mongoose.connect('mongodb://localhost:27017/sales');
        mongoose.set('debug', true);
        var db = mongoose.connection;
        db.on('error', function () {
            console.log('MongoDB connection error');
        });
        db.once('open', function () {
            console.log('MongoDB connection established');
        });
    } else {

        mongoose.connect('mongodb://localhost:27017/sales');
        // mongoose.set('debug', true);
        var db = mongoose.connection;
        db.on('error', function (err) {
            console.log('MongoDB connection error' + err);
        });
        db.once('open', function () {
            console.log('MongoDB connection established');
        });
    }

    // mongodb connection ends

    // enabled CORS for dev only
    if (config.ENV === constants.environments.DEVELOPEMENT) {
        app.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", req.get('Origin'));
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Cache-Control, Pragma");
            res.header('Access-Control-Allow-Credentials', true);
            next();
        });
    }

    //app listen
    app.listen(config.PORT || 3001, function () {
        console.log('sales App running at port :', config.PORT);
    });

    //app.use(express.static(path.join(__dirname, 'client')));
    app.use(express.static(path.join(__dirname, 'public')));

    app.set('superSecret', config.JWT_SECRET);
    app.use(morgan('dev')); // loging api requests
    app.use(morgan(' :method :url :response-time :status ')); // loging api requests
    app.use(favicon(__dirname + '/public/favicon.ico'));

    app.use(passport.initialize());
    app.use(passport.session());
    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        limit: '5mb',
        extended: true
    }));

    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');



    // add your route files in this array

    app.use('/api/v1/', [users, roles, orders, spaces]);

    // error handler
    app.use(function (err, req, res, next) {
        console.error(err);
        if (err.code === 11000) {
            return res.status(400).json(err);
        }
        if (err.stack && config.ENV === constants.environments.DEVELOPEMENT) {
            console.error(err, err.stack);

        }
        if (err.name === "CastError") {
            return res.status(404).json(Error.notFound);
        }
        return res.status(err.status || 500).send({
            message: err.message || err.name || err
        });

    });
}
module.exports = app;

function validateCookieToken(req, res, next) {

    if (req.user || (req.cookies && req.cookies.accessToken)) {

        UserController.findByToken(req.cookies.accessToken, function (err) {
            if (err) next(403);
            else {
                next();
            }
        });
    } else {
        next(401);
    }

}