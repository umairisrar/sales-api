var express = require('express');
var router = express.Router();
var UserController = require('../controllers/userController');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var authUtil = require('../utils/auth');
var attach = require('../utils/upload');
var router = express.Router();


passport.use(new BearerStrategy(UserController.findByToken));
var tokenAuth = passport.authenticate('bearer', {
    session: false
});

router.route('/account/login')
    .post(UserController.login);

router.route('/account/changePassword')
    .put(tokenAuth, UserController.changePassword);

router.route('/account/signUp')
    .post(UserController.signUpUser);

router.route('/account/forgetPassword')
    .post(UserController.forgetPassword);

router.route('/account/forgetPassword/update')
    .put(UserController.verifyEmailTokens, UserController.passwordUpdate);

router.route('/account/logout')
    .put(tokenAuth, UserController.logout);

router.route('/users')
    .get(tokenAuth,  UserController.searchUsers);


router.route('/users/:userId')
    .delete(tokenAuth, authUtil.isAdmin, UserController.deleteUser)
    .put(tokenAuth, authUtil.processRole, attach.uploadProfileImage.single('profileImage'), UserController.updateUser)
    .get(tokenAuth, UserController.getUser);

router.route('/users/invites')
    .post(tokenAuth, UserController.createUser);

module.exports = router;