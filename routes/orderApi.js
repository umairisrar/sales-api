var express = require('express');
var router = express.Router();
var UserController = require('../controllers/userController');
var OrderController = require('../controllers/orderController');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
var authUtil = require('../utils/auth');

var router = express.Router();


passport.use(new BearerStrategy(UserController.findByToken));
var tokenAuth = passport.authenticate('bearer', {
    session: false
});

router.route('/orders')
    .get(tokenAuth, OrderController.getOrders)
    // .post(tokenAuth, authUtil.isAdmin, OrderController.createOrUpdateOrder);
    .post(tokenAuth, OrderController.createOrUpdateOrder);

router.route('/orders/:orderId')
    .delete(tokenAuth, OrderController.deleteOrder)
    .get(tokenAuth, OrderController.getOrder)
    .put(tokenAuth, OrderController.createOrUpdateOrder);



module.exports = router;