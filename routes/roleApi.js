var express = require('express');
var router = express.Router();
var UserController = require('../controllers/userController');
var RoleController = require('../controllers/roleController');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
var authUtil = require('../utils/auth');

var router = express.Router();


passport.use(new BearerStrategy(UserController.findByToken));
var tokenAuth = passport.authenticate('bearer', {
    session: false
});

router.route('/roles')
    .get(tokenAuth, RoleController.getRoles)
    .post(tokenAuth, authUtil.isAdmin, RoleController.createRole);

router.route('/roles/:roleId')
    .delete(tokenAuth, authUtil.isAdmin, RoleController.deleteRole)
    .get(tokenAuth, RoleController.getRole)
    .put(tokenAuth, authUtil.isAdmin, RoleController.updateRole);



module.exports = router;