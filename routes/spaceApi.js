var express = require('express');
var router = express.Router();
var UserController = require('../controllers/userController');
var SpaceController = require('../controllers/spaceController');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
var authUtil = require('../utils/auth');

var router = express.Router();


passport.use(new BearerStrategy(UserController.findByToken));
var tokenAuth = passport.authenticate('bearer', {
    session: false
});

router.route('/spaces')
    .get(tokenAuth, SpaceController.getSpaces)
    // .post(tokenAuth, authUtil.isAdmin, SpaceController.createOrUpdateSpace);
    .post(tokenAuth, SpaceController.createOrUpdateSpace);

router.route('/spaces/:spaceId')
    .delete(tokenAuth, SpaceController.deleteSpace)
    .get(tokenAuth, SpaceController.getSpace)
    .put(tokenAuth, SpaceController.createOrUpdateSpace);



module.exports = router;