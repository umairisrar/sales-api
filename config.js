var moment = require('moment');
var date = moment();
var dir = date.year() + "/" + date.format('MM');
var path = 'private/attachments/';
var destination = './private/attachments/';
module.exports = {

  
    JWT_SECRET: process.env.JWT_SECRET || "#appsalesSecret#",
    API_VERSION: "1.0.0",
    ENV: process.env.ENV || 'development',
    DB_NAME: process.env.DB_NAME || 'sales',
    PORT: process.env.PORT || 3001,
    baseURL: process.env.BASE_URL || "http://localhost:3001/",
    ATTACHMENT: {
        PATH_USER_IMAGES: path + dir + '/userImages/',
        PATH_SOP: path + dir + '/sops/',
        DESTINATION_USER_IMAGES: destination + dir + '/userImages/',
        DESTINATION_SOP: destination + dir + '/sops/'
    },
    emailServer: {
        user: "sales.app.lhr@gmail.com",
        password: "muslim munir1",
        host: "smtp.gmail.com",
        ssl: true
    },
    email: {
        PURPOSE: {
            Invitation: 'Invitation',
            FORGOTPASSWORD: 'Password Reset'
        },
        InvitationSubject: 'sales App - Invitation to join'
    },
    HASH_OPTIONS: {
        algorithm: 'RSA-SHA512'
    },

    TOKEN_EXPIRIES: {
        FORGET_PASSWORD_TOKEN_EXPIRY: 1800, // 30 mins
        INVITATION_TOKEN_EXPIRY: 172800, // 48 hours
        ACCESS_TOKEN_EXPIRY: process.env.ACCESS_TOKEN_EXPIRY || 8640000000 // 100 days
    },

    PASSWORD_REGEX: '(?=.*?[^a-zA-Z])(?=.*[A-Z]).{8,}',
    EMAIL_REGEX: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
};