var email = require("emailjs/email");
var config = require('../config.js');
var emailServer = config.emailServer;
var mailServer = email.server.connect(emailServer);
var fs = require("fs");
var _ejs = require("ejs");

// send the message and get a callback with an error or details of the message that was sent
module.exports = {
    sendEmail: function (receiver, purpose, token, password, done) {
        if (!receiver) {
            return done({
                message: "email field is emty!"
            });
        }

        var email = {};

        if (config.email.PURPOSE.Invitation === purpose) {
            email.subject = config.email.InvitationSubject;
            email.template = process.cwd() + '/views/inviteEmail.ejs';
            email.baseUrl = config.baseURL + "/register?" + "token=" + token + "&email=" + receiver;
            email.logoUrl = config.baseURL + 'public/assets/images/NextLogo.png';
        } else if (config.email.PURPOSE.FORGOTPASSWORD === purpose) {
            email.subject = config.email.PURPOSE.FORGOTPASSWORD;
            email.template = process.cwd() + '/views/forgetpassword.ejs';
            email.baseUrl = config.baseURL + "/forgetPassword?" + "token=" + token + "&email=" + receiver;
            email.newPassword = password;
        } else {
            return done({
                message: 'no or invalid purpose '
            });
        }

        fs.readFile(email.template, 'utf8', function (err, file) {
            if (err)
                return done(err);
            else {
                //compile jade template into function
                var compiledTmpl = _ejs.compile(file, {
                    filename: email.template
                });
                // get html back as a string with the context applied;
                var html = compiledTmpl({
                    data: {
                        baseurl: email.baseUrl,
                        newPassword: email.newPassword,
                        rootPath: config.baseURL,
                        logoUrl: email.logoUrl
                    }
                });
                mailServer.send({

                    from: 'alpha@salesapp.com',
                    to: receiver,
                    subject: email.subject,
                    attachment: [{
                        data: html,
                        alternative: true
                    }]

                }, done);
            }
        });

    }

};