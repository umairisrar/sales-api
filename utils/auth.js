var _ = require('lodash');
var constants = require('../constants/constants');
var Error = require('./error.messages');
module.exports = {
    isAdmin: function (req, res, next) {
        if (req.user && req.user.role && req.user.role.name === constants.user.roles.ADMIN) {
            return next();
        } else {
            return next(Error.forbidden);
        }
    },
    processRole: function (req, res, next) {
        if (req.user && req.user.role && req.user.role.name === constants.user.roles.ADMIN) {
            next();
        } else if (req.user && req.user.role &&  req.user.id === req.params.userId) {
            req.body = _.omit(req.body, 'active');
            next();
        } else {
            next({
                status: 403,
                message: "UnAuthorized"
            });
        }
    }
};