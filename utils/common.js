var jsonWebToken = require('jsonwebtoken');
var config = require('../config');
var _ = require('lodash');
var randomString = require('random-string');
var constants = require('../constants/constants');
module.exports = {


    generateToken: function (user, expiry, callback) {
        // user must have id and email for safe encoding
        //this util will generate a unique token
        if (!expiry) {
            expiry = config.INVITATION_TOKEN_EXPIRY;
        }

        if (!(user.email && user.id)) {
            callback({
                message: 'user email and id are required'
            });
        }
        return callback(null, jsonWebToken.sign({
            id: user.id,
            email: user.email
        }, config.JWT_SECRET, {
                expiresIn: expiry
            }));
    },
    partialUpdate: function (subDoc, partial) {

        return _.mapKeys(partial, function (value, key) {
            return subDoc + '.$' + key;
        });
    }


};