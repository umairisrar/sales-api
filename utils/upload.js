var multer = require('multer');
var crypto = require('crypto');
var path = require('path');
var config = require('../config');
var uuid = require('node-uuid');
var moment = require('moment');
var attachment = multer.diskStorage({

    destination: config.ATTACHMENT.DESTINATION_SOP,
    filename: function(req, file, cb) {
        cb(null, uuid.v4() + path.extname(file.originalname));
    }
});
var profileImage = multer.diskStorage({

    destination: config.ATTACHMENT.DESTINATION_USER_IMAGES,
    filename: function(req, file, cb) {
        cb(null, uuid.v4() + path.extname(file.originalname));

    }
});
module.exports = {
    uploadAttachment: multer({
        fileFilter: function(req, file, cb) {
            if (req.body) {
                file.body = req.body;
            }
            return cb(null, true);
        },
        storage: attachment
    }),
    uploadProfileImage: multer({
        fileFilter: function(req, file, cb) {
            if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/gif') {
                return cb({
                    message: 'Only image files are allowed!'
                });
            }
            // To accept the file pass `true`, like so:
            return cb(null, true);
        },
        storage: profileImage
    })
};