var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var timestamps = require('mongoose-timestamp');
var User = require('./user');
var constants = require('../constants/constants');
var role = new Schema({
    name: {
        type: 'String',
        required: true,
        allowNull: false
    },
    code: {
        type: 'String'
    }

});
role.method('getUserCount', function() {
    return User.count({
        role: this._id,
        deletedAt: null
    }).exec(function(err, result) {
        return result;
    });
});


role.set('toJSON', {
    virtuals: true
});

role.plugin(timestamps); // it will add createdAt and updatedAt and manage them

module.exports = mongoose.model('Role', role);