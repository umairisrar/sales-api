var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passwordHash = require('password-hash');
var timestamps = require('mongoose-timestamp');
var config = require('../config');

var user = new Schema({

    firstName: {
        type: 'String'
    },
    lastName: {
        type: 'String'
    },
    userName: {
        type: 'String',
        unique: true,
        sparse: true
    },
    primaryContact: {
        type: 'String'
    },
    email: {
        type: 'String',
        unique: true,
        required: true,
        email: true
    },
    password: {
        type: 'String'
    },
    accessToken: {
        type: 'String'
    },
    accessTokenCreation: {
        type: 'Date'
    },
    lastLoginDate: {
        type: 'Date'
    },

    active: {
        type: 'Boolean',
        default: false
    },

    role: {
        type: Schema.Types.ObjectId,
        ref: 'Role',
        required: true
    }

});

user.path('password').set(function(newVal) {
    return passwordHash.generate(newVal, config.HASH_OPTIONS);
});
user.path('email').validate(function(value) {
    return config.EMAIL_REGEX.test(value);
}, 'Invalid email');
user.plugin(timestamps); // it will add createdAt and updatedAt and manage them

user.methods.hashPassword = function(password) {
    return passwordHash.generate(password, config.HASH_OPTIONS);
};

user.methods.validatePassword = function(userPass, password) {
    return passwordHash.verify(userPass, password);
};

module.exports = mongoose.model('User', user);