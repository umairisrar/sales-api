var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passwordHash = require('password-hash');
var timestamps = require('mongoose-timestamp');
var config = require('../config');

var order = new Schema({

    name: {
        type: 'String',
        required: true
    },
    account_id: {
        type: 'String',
        required: true
    },
    content: [{
        space_id: {
            type: Schema.Types.ObjectId,
            ref: 'Space',
            required: true
        },
        duration: {
             type: 'String',
            required: true
        },
        price: {
            type: 'Number',
            required: true
        }
    }],
    total_price: {
        type: 'Number'
    },
    isPaid: {
        type: 'Boolean'
    },
    customer_note: {
        type: 'String'
    },
    staff_note: {
        type: 'String'
    }
});

order.plugin(timestamps);
module.exports = mongoose.model('Order', order);