var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passwordHash = require('password-hash');
var timestamps = require('mongoose-timestamp');
var config = require('../config');

var space = new Schema({

    height: {
        type: 'String',
        required: true
    },
    width: {
        type: 'String',
        required: true
    },
    min_height: {
        type: 'String',
        required: true
    },
    max_height: {
        type: 'String',
        required: true
    },
    max_width: {
        type: 'String',
        required: true
    },
    min_width: {
        type: 'String',
        required: true
    },
    mediaType: {
        type: 'String'
    },

    advertType: {
        type: 'String'
    },
    aspectRatio: {
        type: 'String'
    },
    weatherMood: {
        type: 'String'
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }

});

space.plugin(timestamps);
module.exports = mongoose.model('Space', space);