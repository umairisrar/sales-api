var Order = require('../models/order');
var User = require('../models/user');
var _ = require('lodash');
var Promise = require('bluebird');

module.exports = {

    createOrUpdateOrder: function (req, res, next) {
        var orderBody = req.body;
        console.log(orderBody);
        Order.findOne({
            _id: orderBody._id || req.params.orderId
        }).then(function (order) {
            if (!order) {
                orderBody.createdBy = req.user._id;
                return Order.create(orderBody).then(function (order) {
                    return order._id;
                });
            } else {
                return order.update(orderBody).then(function () {
                    return orderBody._id;
                });
            }
        }).then(function (orderId) {
            return Order.findOne({_id: orderId}).then(function (order) {
                res.json(order);
            });
        }).catch(next);
    },
    getOrders: function (req, res, next) {
        var query = {},
            page = parseInt(req.query.page),
            limit = parseInt(req.query.limit);

        if (req.query.search) {
            var search = new RegExp(req.query.search, 'i');
            query.name = search;

        }


        Order.find(query)
            .limit(limit)
            .skip((page - 1) * limit)
            .sort({
                updatedAt: 'desc'
            }).then(function (orders) {
                return Order.count(query).then(function (count) {
                    res.json({
                        orders: orders,
                        count: count
                    });
                });

            }).catch(next);
    },
    getOrder: function (req, res, next) {

        Order.findOne({ //update a particular role with id
            _id: req.params.orderId
        }, function (err, order) {
            if (err)
                return next(err);
            else {
                res.status(200).send(order);
            }
        });
    },
    deleteOrder: function (req, res, next) {

        Order.remove({
            _id: req.params.orderId
        }, function (err) {
            if (err)
                return next(err);
            else {
                res.sendStatus(200);
            }
        });
    }
};