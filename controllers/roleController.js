var Role = require('../models/role');
var User = require('../models/user');
var _ = require('lodash');
var Promise = require('bluebird');
var constants = require('../constants/constants');

module.exports = {
    createRole: function(req, res, next) {

        Role.create(req.body).then(function(role) {
            return res.status(201).send(role);
        }).catch(next);
    },

    getRoles: function(req, res, next) {
        var query = {
                deletedAt: null
            },
            page = parseInt(req.query.page),
            limit = parseInt(req.query.limit);

        if (req.query.search) {
            var search = new RegExp(req.query.search, 'i');
            query.name = search;

        }


        Role.find(query)
            .select('name code associatedUsers')
            .limit(limit)
            .skip((page - 1) * limit)
            .sort({
                updatedAt: 'desc'
            })
            .then(function(roles) {
                return Promise.map(roles, function(role) {
                    return role.getUserCount().then(function(result) {
                        role = role.toObject();
                        role.associatedUsers = result;
                        return role;
                    });
                }).then(function(roles) {
                    return Role.count(query).then(function(count) {
                        res.json({
                            roles: roles,
                            count: count
                        });
                    });
                });
            }).catch(next);
    },
    updateRole: function(req, res, next) {
        var formData = req.body;
        Role.findOne({
            _id: req.params.roleId
        }).then(function(role) {
            if (role.name === constants.user.roles.ADMIN) {
                role.description = formData.description;
            } else {
                role.name = formData.name;
                role.description = formData.description;
                role.access = formData.access;
            }
            role.save().then(function(rl) {
                if (role.name === constants.user.roles.ADMIN && formData.name) {
                    return res.status(400).json({
                        updateError: "Name of role " + role.name + " can not be updated!"
                    });
                }
                return res.sendStatus(200);
            });
        }).catch(next);
    },

    getRole: function(req, res, next) {

        Role.findOne({ //update a particular role with id 
            _id: req.params.roleId,
            deletedAt: null
        }, req.body).then(function(role) {
            if (role)
                res.status(200).send(role);
            else res.sendStatus(404);
        }).catch(next);
    },

    deleteRole: function(req, res, next) {
        var formData = req.body;
        Role.findOne({
            _id: req.params.roleId
        }).then(function(role) {
            if (role.name === constants.user.roles.ADMIN) {
                return res.sendStatus(400);
            } else
            role.remove().then(function(rl) {
                return res.sendStatus(200);
            });
        }).catch(next);
    }
}

