var Space = require('../models/space');
var User = require('../models/user');
var _ = require('lodash');
var Promise = require('bluebird');

module.exports = {

    createOrUpdateSpace: function (req, res, next) {
        var spaceBody = req.body;
        console.log(spaceBody);
        Space.findOne({
            _id: spaceBody._id || req.params.spaceId
        }).then(function (space) {
            if (!space) {
                spaceBody.createdBy = req.user._id;
                return Space.create(spaceBody).then(function (space) {
                    return space._id;
                });
            } else {
                return space.update(spaceBody).then(function () {
                    return spaceBody._id;
                });
            }
        }).then(function (spaceId) {
            return Space.findOne({_id: spaceId}).then(function (space) {
                res.json(space);
            });
        }).catch(next);
    },
    getSpaces: function (req, res, next) {
        var query = {},
            page = parseInt(req.query.page),
            limit = parseInt(req.query.limit);

        if (req.query.search) {
            var search = new RegExp(req.query.search, 'i');
            query.name = search;

        }


        Space.find(query)
            .limit(limit)
            .skip((page - 1) * limit)
            .sort({
                updatedAt: 'desc'
            }).then(function (spaces) {
                return Space.count(query).then(function (count) {
                    res.json({
                        spaces: spaces,
                        count: count
                    });
                });

            }).catch(next);
    },
    getSpace: function (req, res, next) {

        Space.findOne({ //update a particular role with id
            _id: req.params.spaceId
        }, function (err, space) {
            if (err)
                return next(err);
            else {
                res.status(200).send(space);
            }
        });
    },
    deleteSpace: function (req, res, next) {

        Space.remove({
            _id: req.params.spaceId
        }, function (err) {
            if (err)
                return next(err);
            else {
                res.sendStatus(200);
            }
        });
    }
};