var User = require("../models/user");
var Role = require("../models/role");
var utilEmail = require("../utils/email");
var utilErrors = require("../utils/error.messages");
var utilCommon = require("../utils/common");
var Promise = require('bluebird');
var jsonWebToken = require('jsonwebtoken');
var config = require('../config');
var jwt = require('jwt-simple');
var moment = require('moment');
var passwordHash = require('password-hash');
var async = require('async');
var constants = require('../constants/constants');
var _ = require("lodash");

var Error = require('../utils/error.messages');


module.exports = {
    findByToken: function (token, done) {

        var payload = null;
        try {
            payload = jwt.decode(token, config.JWT_SECRET);

        } catch (e) {
            return done({
                message: 'Bad token'
            });
        }
        if (payload) {
            // check the token expiration
            if ((new Date().getTime() - new Date(payload.created_on).getTime()) >= config.TOKEN_EXPIRIES.ACCESS_TOKEN_EXPIRY) {
                return done(null, {
                    message: 'Token has expired'
                });
            }
            return User.findOne({
                _id: payload.id,
                accessToken:token,
                deletedAt: null,
                active: true
            })
                .select('firstName lastName email userName role active skype primaryContact')
                .populate('role')
                .exec(function (err, user) {
                    if (err)
                        return done(err);
                    else if (user) {
                        return done(null, user);
                    } else {
                        return done(false);
                    }
                });
        } else {
            return done(null, {
                message: 'Bad token'
            });
        }

    },
    login: function (req, res, next) {
        if (req.body.loginType || req.body.userName)
            return loginUtil(req.body, function (err, user) {
                if (err) {
                    return next(err);
                } else if (user) {
                    return res.json(user);
                }
            });
        else {
            return next(utilErrors.missingRequiredQueryParameter);
        }

    },
    logout: function (req, res, next) {
        if (req.user) {
            User.update({
                _id:req.user._id
            }, {
                    accessToken: null,
                    accessTokenCreation: null
                },
                function (err,result) {
                    if (err)
                        return next(err);
                    else {
                        return res.status(200).end();
                    }

                });
        } else {
            res.status(400).send();
        }

    },
    updateUser: function (req, res, next) {
        var body = _.omit(req.body, ['password', 'accessToken', 'accessTokenCreation', 'email', 'lastLoginDate']);
        var userId = req.params.userId;
        if (!userId) {
            next({
                status: 400,
                message: '_id of user is required for update'
            });
        }

        return User.update({
            _id: userId
        }, body)
            .then(function () {

                return findUser(userId, function (err, user) {
                    if (err) {
                        return next(err);
                    } else if (user) {
                        return res.status(200).send(user);
                    }
                });

            })
            .catch(next);
    },
    deleteUser: function (req, res, next) {

        var userId = req.params.userId;
        if (!userId) {
            next({
                status: 400,
                message: '_id of user is required for update'
            });
        }
        User.update({
            _id: userId
        }, {
                active: false
            }, function (err) {
                if (err)
                    next(err);
                else {
                    res.sendStatus(200);
                }
            });

    },


    searchUsers: function (req, res, next) {

        var query = {};
        var page = parseInt(req.query.page),
            limit = parseInt(req.query.limit);

        if (req.query.search) {
            var search = new RegExp(req.query.search, 'i');
            query = {
                $or: [{
                    firstName: search
                }, {
                    lastName: search
                }, {
                    email: search
                }, {
                    skype: search
                }]
            };
        }
        if (!_.isUndefined(req.query.active)) {
            query.active = req.query.active;
        }
        query.deletedAt = null;
        if (req.query.currentUser) {
            query = _.extend(query, {
                _id: {
                    $ne: req.user._id
                }
            });
        }
        User.find(query)
            .select('firstName lastName email userName role active skype primaryContact')
            .populate('role')
            .limit(limit)
            .skip((page - 1) * limit)
            .sort({
                updatedAt: 'desc'
            })
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                } else {

                    return User.count(query, function (err, count) {
                        if (err) {
                            return next(err);
                        } else {
                            return res.json({
                                users: users,
                                count: count
                            });
                        }

                    });

                }
            });
    },
    getUser: function (req, res, next) {
        var query = {};

        if (req.params.userEmail) {
            query = {
                email: req.params.userEmail
            };
        } else if (req.params.userId) {
            query = {
                _id: req.params.userId
            };
        } else {
            return next({
                message: 'undefined params'
            });
        }
        query.deletedAt = null;
        User.findOne(query, {
            firstName: true,
            lastName: true,
            userName: true,
            email: true,
            skype: true,
            primaryContact: true

        })
            .select('firstName lastName email userName role active skype primaryContact')
            .populate('role')
            .exec(function (err, user) {
                if (err) {
                    return next(err);
                }

                return res.json(user);
            });
    },
    signUpUser: function (req, res, next) {
        var user = req.user;
        var body = req.body;

        if (!req.body.email) {
            return res.status(400).send({
                message: 'email is required!'
            });
        }
        if (!req.body.password || req.body.password.match(config.PASSWORD_REGEX) === null) {
            return res.status(400).send({
                message: 'Password must have minimum 8 characters having at least one capital character and one number.'
            });
        }
        if (!req.body.userName) {
            return res.status(400).send({
                message: 'userName is required attribute'
            });
        }
        Role.findOne({ name: constants.user.roles.USER }).then(function (role) {
            if (!role) {
                return res.status(400).send({
                    message: 'User role not found'
                });
            }
            return User.create(
                {
                    password: req.body.password,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    userName: req.body.userName,
                    email: req.body.email,
                    primaryContact: req.body.primaryContact,
                    active: true,
                    role: role._id
                }).then(function (result) {

                    return res.status(200).send({
                        message: 'User Registered'
                    });

                }).catch(next);
        });




    },
    /*signUpUser: function (req, res, next) {
     var user = req.user;
     var body = req.body;

     if (!req.body.email) {
     return res.status(400).send({
     message: 'email is required!'
     });
     }
     if (!req.body.password || req.body.password.match(config.PASSWORD_REGEX) === null) {
     return res.status(400).send({
     message: 'Password must have minimum 8 characters having at least one capital character and one number.'
     });
     }
     *//*  if (!req.body.userName) {
 return res.status(400).send({
 message: 'userName is required attribute'
 });
 }*//*
       User.update({
       _id: user._id,
       active: false
       }, {
       password: passwordHash.generate(req.body.password, config.HASH_OPTIONS),
       firstName: req.body.firstName,
       lastName: req.body.lastName,
       //userName: req.body.userName,
       skype: req.body.skype,
       primaryContact: req.body.primaryContact,
       active: true
       }, function (err, result) {
       if (err) {
       return next(err);
       } else if (result.nModified <= 0) {
       return next({
       status: 400,
       message: 'user already signed up'
       });
       } else {
       var userObject = {};
       userObject.password = body.password;
       userObject.loginType = body.email;
       return loginUtil(userObject, function (err, user) {
       if (err) {
       return res.status(401).end(err);
       } else if (user) {
       return res.status(200).send(user);
       }
       });
       }
       });
       },
       sendInvitation: function (req, res, next) {
       var user = req.body;
   
       var invitedToken = null;
       if (_.isEmpty(user))
       return next({
       message: 'user is required'
       });
   
   
       var InvitedUser = null;
       user = _.omit(user, ['password', 'accessToken', 'accessTokenCreation', 'lastLoginDate', 'deletedAt']);
       user.password = passwordHash.generate(constants.user.defaultPassword, config.HASH_OPTIONS);
       user.active = false;
       user.deletedAt = null;
       user.role = user.role;
       var email = user.email;
       if (!config.EMAIL_REGEX.test(email)) {
       return next({
       email: email,
       token: null,
       message: "invalid email"
       });
       } else {
   
       async.series({
       createUser: function (callback) {
       User.findOne({
       email: email
       }, function (err, userObj) {
       if (err)
       return callback(err);
       else if (userObj) {
       if (userObj.deletedAt === null) {
       return callback({
       message: 'this user already exists',
       status: 400
       });
       } else {
       userObj.deletedAt = null;
   
       _.forIn(user, function (value, key) {
       userObj[key] = value;
       });
       userObj.save(function (err) {
       if (err)
       return callback(err);
       else {
       InvitedUser = userObj.toObject();
       return callback(null, userObj);
       }
       });
       }
   
       } else {
       return User.create(user, function (err, user) {
       if (err)
       return callback(err);
       else {
       InvitedUser = user.toObject();
       return callback(null, user);
       }
       });
       }
       });
       },
       sendMail: function (callback) {
       utilCommon.generateToken({
       id: InvitedUser._id,
       email: email
       }, config.TOKEN_EXPIRIES.INVITATION_TOKEN_EXPIRY, function (err, token) {
       if (err && !token)
       return callback(err);
       else {
       invitedToken = token;
       return utilEmail.sendEmail(email, config.email.PURPOSE.Invitation, token, null, callback);
       }
       });
   
       }
       }, function (err) {
       if (err) {
   
       return next({
       email: user,
       token: null,
       message: err
       });
   
       } else {
       return res.json({
       user: InvitedUser,
       token: invitedToken
       });
       }
       });
       }
   
   
       },*/
    createUser: function (req, res, next) {
        var body = _.omit(req.body, ['password', 'accessToken', 'accessTokenCreation', 'lastLoginDate']);
        body.active = true;
        body.password = 'Abcd1234';
        return User.findOne({ email: body.email }).then(function (user) {
            if (!user) {
                return User.create(body)
                    .then(function (user) {
                        return res.status(200).send(user);
                    })
            } else {
                return res.status(400).send({ message: "user  already exists" });
            }
        }).catch(next);
    },
    verifyEmailTokens: function (req, res, next) {
        var invitationToken = req.body.token || req.query.token;

        if (!invitationToken)
            return next({
                status: 403,
                message: 'Acess Denied'
            });
        // verifies secret and checks exp
        jsonWebToken.verify(invitationToken, config.JWT_SECRET, function (err, decoded) {
            if (err && !decoded) {
                return next(err);
            } else {
                User.findOne({
                    _id: decoded.id,
                    email: decoded.email,
                    deletedAt: null
                }, function (err, user) {
                    if (err)
                        next(err);
                    else {
                        req.user = user;
                        next();
                    }
                });
            }
        });


    },
    changePassword: function (req, res, next) {
        var body = req.body;
        if (!(body.oldPassword && body.newPassword && body.newConfirmPassword)) {
            return next(Error.inValidPassword);
        } else if (body.newPassword !== body.newConfirmPassword) {
            return next(Error.inValidPassword);
        } else {
            User.findOne({
                _id: req.user._id
            }, function (err, user) {
                if (err)
                    return next(err);
                else if (user && user.validatePassword(body.oldPassword, user.password)) {
                    user.update({
                        password: user.hashPassword(body.newPassword)
                    }, function (err) {
                        if (err)
                            return next(err);
                        else {
                            res.sendStatus(200);
                        }
                    });
                } else {
                    return next(Error.inValidPassword);
                }

            });
        }

    },
    forgetPassword: function (req, res, next) {
        var email = req.body.email;
        if (!email) {
            return next(Error.inValidEmail);
        } else if (!config.EMAIL_REGEX.test(email)) {
            return next(Error.inValidEmail);
        }
        var newPassword = utilCommon.generatePassword();

        User.findOne({
            email: email,
            active: true
        }, function (err, user) {
            if (err)
                return next(err);
            else if (!user) {
                return next(Error.inValidEmail);
            } else {
                user.update({
                    password: user.hashPassword(newPassword)
                }, function (err, result) {

                    if (err)
                        return next(err);

                    //send mail to user 
                    utilCommon.generateToken({
                        id: user._id,
                        email: email
                    }, config.TOKEN_EXPIRIES.FORGET_PASSWORD_TOKEN_EXPIRY, function (err, token) {
                        if (err)
                            return next(err);
                        else {
                            return utilEmail.sendEmail(email, config.email.PURPOSE.FORGOTPASSWORD, token, newPassword, function (err) {
                                if (err)
                                    next(err);
                                else {
                                    res.status(200).send({
                                        email: email,
                                        token: token
                                    });
                                }
                            });
                        }
                    });

                });
            }

        });
    },
    passwordUpdate: function (req, res, next) {

        var body = req.body;
        var user = req.user;

        if (!(body.email && body.newPassword && body.newConfirmPassword)) {
            return next({
                message: 'email ,  new and confirm passwords are missing '
            });
        } else if (body.newPassword !== body.newConfirmPassword) {
            return next({
                message: 'passwords are not matching'
            });
        } else if (body.email !== req.user.email) {
            return next({
                message: 'wrong credentials'
            });
        }
        user.update({
            password: user.hashPassword(body.newPassword)
        }, function (err) {
            if (err)
                return next(err);
            else {
                res.sendStatus(200);
            }
        });

    }


};

function loginUtil(userBody, done) {
    console.log(userBody)
    if (userBody && userBody.loginType) {

        User.findOne({
            $or: [{
                email: new RegExp(userBody.loginType, 'i')
            }, {
                userName: new RegExp(userBody.loginType, 'i')
            }]
            // active: true
        },
            function (err, user) {

                if (err) {
                    return done(err);
                } else if (!user)
                    return done(utilErrors.authenticationFail);
                else if (!user.active) {
                    return done(utilErrors.inActiveUser);
                }
                else if (user && user.validatePassword(userBody.password, user.password)) { // password matches

                    if (!user.accessToken) { // if user has no token

                        var accessTokenCreation = new Date();
                        var accessToken = jwt.encode({
                            id: user.id,
                            created_on: accessTokenCreation
                        }, config.JWT_SECRET);

                        return user.update({
                            accessToken: accessToken,
                            accessTokenCreation: accessTokenCreation,
                            lastLoginDate: new Date()
                        },
                            function (err) { // update user accesstoken
                                if (err) {
                                    return done(err);
                                } else {
                                    return findUser(user._id, done);
                                }
                            });

                    } else { // if user has a token already 

                        user.update({
                            lastLoginDate: new Date()
                        }, function (err) {
                            if (err)
                                return done(err);
                            else {
                                return findUser(user._id, done);
                            }
                        });
                    }

                } else {
                    done(utilErrors.authenticationFail);
                }
            });
    } else {
        done(utilErrors.authenticationFail);
    }
}

function findUser(userId, done) {
    return User.findOne({
        _id: userId,
        deletedAt: null
    })
        .select('firstName lastName email userName accessToken role active skype primaryContact')
        .populate('role')
        .exec(function (err, user) {
            if (err) {
                return done(err);
            }
            return done(null, user);
        });
}